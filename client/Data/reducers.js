import { combineReducers } from 'redux'
import { FETCHED_PAGE, setPage } from './actions.js'

export default function appReducer(state = {}, action){
    switch(action.type){
        case FETCHED_PAGE:
            return {
                page: action.page,
                data: action.posts
            }
    }
}