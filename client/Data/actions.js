export const FETCHED_PAGE = "FETCHED_PAGE";

export function buildPage(page, posts) {
    return {
        type: FETCHED_PAGE,
        page,
        posts
    };
}