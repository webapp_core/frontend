import {Link} from "react-router-dom";
import React from "react";
import styles from "../Style/header.css";
import classNames from "classnames";

export default class Header extends React.Component{
    
    constructor(props){
        super(props);
    }
    
    render(){
        return(
            <div>
                <Link to='/' className={classNames("header_title_50", "header_border_right")}>Main</Link>
                <Link to='/about' className={classNames("header_title_50", "header_border_left")}>About</Link>
            </div>
        );
    }
}