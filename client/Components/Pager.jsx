import React from "react";
import styles from "../Style/pager.css";

const _bttnClass = "pagerStep"

export default class Pager extends React.Component{
    
    constructor(params){
        super(params);
    }

    render(){
        return(
            <div className={styles.pager_container}>
                <button className={ _bttnClass } onClick={() => this.props.clickHandle("back")} name="previous">{"<< Previous"}</button>
                <button className= {_bttnClass } onClick={() => this.props.clickHandle("next")} name="next">{"Next >>"}</button>
            </div>
        );
    }
}