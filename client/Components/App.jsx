import React from "react";
import {Route, Switch} from 'react-router-dom';
import Main from "./Main.jsx";
import About from "./About.jsx";


export default class App extends React.Component {
    render(){
        return (
            <main>
                <Switch>
                    <Route exact path="/about" component={About}/>                 
                    <Route path="/" component={Main}/>
                </Switch>
            </main>
        );
    }
}