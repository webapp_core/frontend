import React from "react";
import styles from "../Style/post.css";

export default class Post extends React.Component{
    
    constructor(props){
        super(props);
    }
    
    render(){
        return(<div>
            <div className={styles.p_title}>
                {this.props.post.title} {this.props.post.created.substring(0,this.props.post.created.indexOf("T"))}
            </div>
            <div className={styles.p_lead}>TL;DR {this.props.post.lead}</div>
            <div className={styles.p_text}>{this.props.post.text}</div>
        </div>);
    }
}