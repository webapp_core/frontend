import React from "react";
import Post from "./Post.jsx"

export default class Container extends React.Component {
    
    constructor(props){
        super(props);
    }
    
    render() {
        const res = this.props.data.map(function(post){
            return <Post key ={post.id} post = {post}/>
        })
        
        return (<div>{res}</div>
        );
    }
}