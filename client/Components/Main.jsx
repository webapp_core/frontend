import React from "react";
import Header from "./Header.jsx";
import Pager from "./Pager.jsx";
import Container from "./Container.jsx";
import $ from "jquery";
import styles from "../Style/main.css";

const pageSize = 5;

export default class Main extends React.Component {
    
    constructor(params){
        super(params);
        this.getData = this.getData.bind(this);
        this.getPage = this.getPage.bind(this);

        this.state = {
            page: 1,
            data: []
        };
    }

    componentDidMount(){
        const pageIn = this.state.page;
        this.getData(pageIn);
    }

    getData(pageIn){
        $.ajax({
            url:`http://pr-blg-api.azurewebsites.net/api/post/${pageIn}/${pageSize}`,
            crossDomain: true
        }).then(function(res) {
            this.setState({
            page: pageIn,
            data: res
        })}.bind(this));
    }

    getPage(val){
        let newPage;  
        
        switch(val){
            case "next":
                newPage = this.state.page + 1;            
                break;
            case "back":
                newPage = this.state.page - 1;                               
                break;    
        }

        if(newPage < 1){
            newPage = 1;
        }

        this.setState({
            page: newPage,
            data: this.getData(newPage)
        });
    }
    

    render(){
        let conte = "";
        if(this.state.data !== undefined){
            conte = <Container data = {this.state.data}/>
        }
         
        return(
            <div className={styles.top_container}>
                <Header/>
                {conte}
                <Pager clickHandle = {this.getPage}/>
            </div>
        );
    }
}