import React from 'react';
import {render} from 'react-dom';
import App from './components/App.jsx';
import {BrowserRouter, Route} from 'react-router-dom';
import {createStore} from "redux";
import appReducer from "./Data/reducers.js";

let store = createStore(appReducer);

render(
    <BrowserRouter>
       <App/>
    </BrowserRouter>, document.getElementById('root'));