using Microsoft.AspNetCore.Mvc;

namespace server.Controllers {
    public class SpaController: Controller {

        public IActionResult Index() {
            return File("~/index.html", "text/html");
        }
    }
}
