var path = require('path');
var webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const combineLoaders = require('webpack-combine-loaders');

    
module.exports = {
    entry: './client/index.js',
    output: {
        path: path.resolve('server/wwwroot'),
        filename: 'index_bundle.js'
      },
      plugins: [
        new HtmlWebpackPlugin({
        template: './client/index.html',
        filename: 'index.html',
        inject: 'body'
      }),
      new ExtractTextPlugin('styles.css')      
      ],      
      module: {
        loaders: [
          { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
          { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ },
          {
            test: /\.css$/,
            loader: combineLoaders([
              {
                loader: 'style-loader'
              }, {
                loader: 'css-loader',
                query: {
                  modules: true,
                  localIdentName: '[name]_[local]'
                }
              }
        ])
      }
    ]
    },
    stats: {
        colors: true
    },
    devtool: 'source-map'    
};